import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import * as app from "application";
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { RouterExtensions } from "nativescript-angular/router";
import { filter } from "rxjs/operators";
import { PlatformService } from './services/platform.service'; 

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
})
export class AppComponent implements OnInit, OnDestroy { 
    private _activatedURL: string;
    private _sideDrawerTransition: DrawerTransitionBase;

    constructor(private router: Router, private routerExtensions: RouterExtensions,
        private platformService: PlatformService) {
        //services are injected in the component constructor
    }

    ngOnInit(): void {
        this._activatedURL = "/menu";
        this._sideDrawerTransition = new SlideInOnTopTransition();

        this.router.events
            .pipe(filter((event: any) => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => this._activatedURL = event.urlAfterRedirects);

        this.platformService.printPlatformInfo(); 
        this.platformService.startMonitoringNetwork()
        .subscribe((message: string) => {
            console.log(message); 
            
        });
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    isComponentSelected(url: string): boolean {
        return this._activatedURL === url;
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {transition: {
            name: "fade"
        }
    });

    
    const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();

    }

    ngOnDestroy() { 
    
        this.platformService.stopMonitoringNetwork(); 
        
    } 
}
