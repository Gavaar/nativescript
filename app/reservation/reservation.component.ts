import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { TextField } from 'ui/text-field';
import { Switch } from 'ui/switch';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ReservationModalComponent } from "../reservationmodal/reservationmodal.component";
import { RouterExtensions } from 'nativescript-angular/router';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Page } from 'ui/page';
import { View } from 'ui/core/view';
import * as app from "application";
import { CouchbaseService } from '../services/couchbase.service';

@Component({
    selector: 'app-reservation',
    moduleId: module.id,
    templateUrl: './reservation.component.html',
    styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

    docId: string = "reservations";
    reservationCouch: Array<string>;
    reservation: FormGroup;
    reserveView: View;
    afterSubmit: View = <View>this.page.getViewById<View>('afterSubmit');
    showAfterSubmit: boolean = false;

    constructor(private formBuilder: FormBuilder,
        private modalService: ModalDialogService,
        private vcRef: ViewContainerRef,
        private routerExtensions: RouterExtensions,
        private page: Page,
        private couchbase: CouchbaseService) { 
        
        this.reservation = this.formBuilder.group({
            guests: 3,
            smoking: false,
            dateTime: ['', Validators.required]
        });

        this.reservationCouch = [];

        let doc = this.couchbase.getDocument(this.docId);
        if( doc == null) {
            this.couchbase.createDocument({"reservations": []}, this.docId);
        }
        else {
            this.reservationCouch = doc.reservations;
        }
     }

    ngOnInit() {
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onSmokingChecked(args) { 
        let smokingSwitch = <Switch>args.object;
        if (smokingSwitch.checked) {
            this.reservation.patchValue({smoking: true});
        }
        else {
            this.reservation.patchValue({smoking: false});
        }
    }

    onGuestChange(args) {

        let textField = <TextField>args.object;

        this.reservation.patchValue({guests: textField.text});
    }

    onDateTimeChange(args) {

        let textField = <TextField>args.object;

        this.reservation.patchValue({dateTime: textField.text})
    }
    
    createModalView(args) {

        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: args,
            fullscreen: false
        };

        this.modalService.showModal(ReservationModalComponent, options)
            .then((result: any) => {
                if (args === "guest") {
                    this.reservation.patchValue({guests: result});
                }
                else if (args === "date-time") {
                    this.reservation.patchValue({ dateTime: result});
                }
            });
    }

    onSubmit() {
        console.log('we are sending to the DB: ' + JSON.stringify(this.reservation.value));
        this.changeView();
    }

    changeView() {
        this.reserveView = <View>this.page.getViewById<View>('reserveView');
        this.afterSubmit = <View>this.page.getViewById<View>('afterSubmit');

        this.afterSubmit.animate({
            scale: { x: 0, y: 0 },
            duration: 200
        });

        this.reserveView.animate({
            scale: { x: 0.1, y: 0.1},
            duration: 500,
            opacity: 0
        })
        .then(() => {
            console.log("Pre-Couchbase: " + this.reservationCouch);
            let reserve = this.reservation.value;
            this.reservationCouch.push(reserve);
            this.couchbase.updateDocument(this.docId, {"reservations": this.reservationCouch});
            console.log("Post-Couchbase: " + this.reservationCouch);

            this.showAfterSubmit = true;
            this.afterSubmit.animate({
                scale: { x: 1, y: 1 },
                opacity: 1,
                duration: 500
            }).then(() => console.log("success"));
        });
    }

    goBack(): void {
        this.routerExtensions.back();
    }
}